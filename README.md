# Waves Ducks Smart Contract Audit

## Introduction

### General Provisions

The Waves team asked me team to audit their Waves Ducks smart contracts. The code was located in the following github repository: https://github.com/vlzhr/waves-ducks.

### Scope of the Audit

The scope of the audit are smart contracts at https://github.com/vlzhr/waves-ducks/tree/bf6c676236dd9cabbfd28d2bc3ed8b31da770417:
* [ducks-incubator.ride](https://github.com/vlzhr/waves-ducks/blob/bf6c676236dd9cabbfd28d2bc3ed8b31da770417/ducks-incubator.ride)
* [ducks-breeder.ride](https://github.com/vlzhr/waves-ducks/blob/bf6c676236dd9cabbfd28d2bc3ed8b31da770417/ducks-breeder.ride)
* [farming.ride](https://github.com/vlzhr/waves-ducks/blob/bf6c676236dd9cabbfd28d2bc3ed8b31da770417/farming.ride)
* [ducks-auction.ride](https://github.com/vlzhr/waves-ducks/blob/bf6c676236dd9cabbfd28d2bc3ed8b31da770417/ducks-auction.ride)

Audited commit is `bf6c676236dd9cabbfd28d2bc3ed8b31da770417`.


## Security Assessment Principles

### Classification of Issues

* CRITICAL: Bugs leading to Waves or token theft, fund access locking or any other loss of Waves/tokens to be transferred to any party (for example, dividends). 

* MAJOR: Bugs that can trigger a contract failure. Further recovery is possible only by manual modification of the contract state or replacement. 

* WARNINGS: Bugs that can break the intended contract logic or expose it to DoS attacks. 

* COMMENTS: Other issues and recommendations.


### Security Assessment Methodology

Stages of the audit were as follows:

* "Blind" manual check of the code and its model 
* "Guided" manual code review
* Checking the code compliance to customer requirements
* Report preparation


## Detected Issues

### CRITICAL :fire:

1.  [farming.ride:216](https://github.com/vlzhr/waves-ducks/blob/bf6c676236dd9cabbfd28d2bc3ed8b31da770417/farming.ride#L216)

    There's no verification of asset issuer. Attacker can stake any NFT created by himself as a jackpot with maximum farming power (100). There are two conditions for an attack:
    * suitable asset name (for instance, `DUCK-AAAAAAAA-GU`)
    * empty perch of any color

    PoC: https://wavesexplorer.com/tx/APZkRsJxTDcJXVPjuhRx8egdnU3grqKR1Yupm461T5uP.
    
    I highly recommend to check that payment asset is issued by incubator or breeding contract.

    _Fixed in https://github.com/vlzhr/waves-ducks/commit/cd4c66e5d28c5c5e8b9ed95bccb15acd134f7531._


### MAJOR

None found.


### WARNINGS

None found.


### COMMENTS

1.  [farming.ride:65](https://github.com/vlzhr/waves-ducks/blob/bf6c676236dd9cabbfd28d2bc3ed8b31da770417/farming.ride#L65)

    It would be more user-friendly to provide meaningful error messages.

1.  [farming.ride:66](https://github.com/vlzhr/waves-ducks/blob/bf6c676236dd9cabbfd28d2bc3ed8b31da770417/farming.ride#L66)

    In case of not using magic numbers code will be more self-documented and readable.

1.  [farming.ride:72](https://github.com/vlzhr/waves-ducks/blob/bf6c676236dd9cabbfd28d2bc3ed8b31da770417/farming.ride#L72)

    I recommend to wrap dynamically generated data storage keys obtaining to functions and statically keys to variables to avoid typos.

1. [farming.ride:143](https://github.com/vlzhr/waves-ducks/blob/bf6c676236dd9cabbfd28d2bc3ed8b31da770417/farming.ride#L143)

    Wrong comment.


1.  [farming.ride:292](https://github.com/vlzhr/waves-ducks/blob/bf6c676236dd9cabbfd28d2bc3ed8b31da770417/farming.ride#L292)

    The smart contract is not locked.
    I recommend to lock it for greater user confidence.


## CONCLUSION

I have identified 1 high risk vulnerability in the audited smart contract. Overall code quality is on a high level.

